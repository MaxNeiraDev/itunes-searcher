package test.cl.itunessearcher.rest.restClient

import android.content.Context
import android.net.ConnectivityManager

import java.io.IOException
import java.util.concurrent.TimeUnit

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import test.cl.itunessearcher.rest.interfaces.ServicesCalls
import test.cl.itunessearcher.rest.restConsumer.RestClientConsumer
import okhttp3.*
import okhttp3.OkHttpClient
import test.cl.itunessearcher.views.customViews.CustomFragment

class RestClient : Callback<ResponseBody> {

    private var triggeringFragment: CustomFragment? = null
    private var lastCalledServiceId: Int ? = 0
    private var lastEndedCallRequestId: Int ? = 0
    private val CONNECT_TIMEOUT_SECONDS = 120
    private val WRITE_TIMEOUT_SECONDS = 120
    private val READ_TIMEOUT_SECONDS = 120

    private var httpClient: OkHttpClient? = null
    data class CustomResult<A, B, C, D>(val successResponse: A, val errorResponse: B, val requestResponseCode: C, val requestId: D)
    private var connectivityManager: ConnectivityManager? = null
    private var parameters = arrayListOf<String>()
    private val callsInExecution = ArrayList<HashMap<Int, String?>>()
    private val servicesErrorMessage = "WS error"
    private val BASE_URL = "https://itunes.apple.com/"

    override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
        var message = ""
        var isRequestedCancellation = false
        t?.let {
            message = if (t.message?.toLowerCase() == "canceled" || t.message!!.toLowerCase().contains("canceled")){
                isRequestedCancellation = true
                "Servicios cancelados."
            } else { servicesErrorMessage }
        } ?:run{
            message = servicesErrorMessage
        }
        RestClientConsumer.connectionError(message, isRequestedCancellation)
    }

    override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?){
        try {
            val responseBodySuccess = response?.body()
            val responseBodyError = response?.errorBody()
            var successResponse = ""
            var errorResponse = ""
            var responseCode:Int ?= 0

            if (responseBodySuccess != null)
                successResponse = responseBodySuccess.string()

            if (responseBodyError != null)
                errorResponse = responseBodyError.string()

            responseCode = response?.code()

            var indexToRemove = -1
            callsInExecution.forEachIndexed {
                index, element ->
                for ((key, value) in element) {
                    if(value!!.isNotEmpty() && call!= null && value==call.request().url().toString()){
                        lastEndedCallRequestId = key
                        indexToRemove = index
                    }
                }
            }
            if (indexToRemove != -1)
                callsInExecution.removeAt(indexToRemove)

            responseHandler(successResponse, errorResponse, responseCode)

        } catch (exception: Exception){
            when (exception){
                is NullPointerException -> exception.printStackTrace()
                is IOException -> exception.message
            }
        }
    }

    private fun isOnline(): Boolean {
        var isOnline = false
        if (triggeringFragment != null && connectivityManager == null)
            connectivityManager = triggeringFragment?.context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        if (connectivityManager != null) {
            val netInfo = connectivityManager?.activeNetworkInfo
            isOnline = netInfo != null && netInfo.isConnectedOrConnecting.or(false)
        }

        return isOnline
    }

    private fun setupClient(consumer: RestClientConsumer?){
        val myBuilder = OkHttpClient.Builder()
        httpClient = myBuilder
                .connectTimeout(CONNECT_TIMEOUT_SECONDS.toLong(), TimeUnit.SECONDS)
                .writeTimeout(WRITE_TIMEOUT_SECONDS.toLong(), TimeUnit.SECONDS)
                .readTimeout(READ_TIMEOUT_SECONDS.toLong(), TimeUnit.SECONDS)
                .build()
    }

    fun serviceCall(serviceId: Int, requestInputs: ArrayList<String>, launcherFragment: CustomFragment){
        for (element in requestInputs) { parameters.add(element) }
        setupCall(serviceId, launcherFragment)
    }

    private fun setupCall(serviceId: Int, launcherFragment: CustomFragment){
        triggeringFragment = launcherFragment
        if(httpClient == null) { setupClient(null) }
        val retrofitBuilder = Retrofit.Builder()
        val retrofitAdapter = retrofitBuilder.baseUrl(BASE_URL).client(httpClient!!).addConverterFactory(GsonConverterFactory.create()).build()
        val call = getCall(serviceId, retrofitAdapter)

        val newCallInfo = HashMap<Int, String?>()
        newCallInfo[serviceId] = if (call != null) call.request()?.url().toString() else ""
        callsInExecution.add(newCallInfo)
        if (isOnline()) { call!!.enqueue(this) }else{ RestClientConsumer.connectionError(null, false) }
        parameters.clear()
    }

    private fun getCall(serviceId: Int, adapter: Retrofit): Call<ResponseBody>? {
        var call: Call<ResponseBody>? = null
        lastCalledServiceId = serviceId
        val servicesInterface = adapter.create<ServicesCalls>(ServicesCalls::class.java)
        when (lastCalledServiceId) {
            RestClientConsumer.getSongInformationRequestId -> {
                call = servicesInterface.getProductLongDescription(parameters[0], "music", parameters[1], "20")
            }
            RestClientConsumer.getAlbumInformationRequestId -> {
                call = servicesInterface.getAlbumInfo(parameters[0], "song")
            }
            else -> {}
        }
        return call
    }

    private fun responseHandler(successBody: String, errorBody: String, code: Int?){
        parameters.clear()
        RestClientConsumer.universalCallBack(CustomResult(successBody, errorBody, code!!, lastEndedCallRequestId!!))
    }

}

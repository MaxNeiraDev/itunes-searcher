package test.cl.itunessearcher.rest.interfaces

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface ServicesCalls {

    @Headers("Content-Type: application/json")
    @GET("search")
    fun getProductLongDescription(
            @Query("term") term: String,
            @Query("mediaType", encoded = true) mediaType:String,
            @Query("offset") offset:String,
            @Query("limit") limit:String ):
            Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @GET("lookup")
    fun getAlbumInfo(@Query("id") id: String, @Query("entity") entity:String): Call<ResponseBody>
}

package test.cl.itunessearcher.rest.restConsumer

import android.annotation.SuppressLint
import test.cl.itunessearcher.managers.LoadingDialogManager
import test.cl.itunessearcher.rest.jsonFormatter.JsonManager
import test.cl.itunessearcher.rest.restClient.RestClient
import test.cl.itunessearcher.views.customViews.CustomFragment
import test.cl.itunessearcher.views.fragments.albumInfo.view.AlbumInfoFragment
import test.cl.itunessearcher.views.fragments.songsSearch.view.SongSearchFragment

@SuppressLint("StaticFieldLeak")
object RestClientConsumer {

    private var client: RestClient? = null
    private var fragmentTrigger: CustomFragment? = null

    private val parameters = arrayListOf<String>()
    var lastRequestedServiceId = -1

    const val getSongInformationRequestId = 1
    const val getAlbumInformationRequestId = 2

    fun getSongInformation(launcher: CustomFragment, productId: String, offset: String){
        RestClientConsumer.parameters.add(productId)
        RestClientConsumer.parameters.add(offset)
        makeCall(launcher, getSongInformationRequestId)
    }

    fun getAlbumInformation(launcher: CustomFragment, productId: String){
        RestClientConsumer.parameters.add(productId)
        makeCall(launcher, getAlbumInformationRequestId)
    }

    private fun makeCall(launcher: CustomFragment, requestedServiceId: Int){
        fragmentTrigger = launcher
        if (client == null)
            client = RestClient()
        lastRequestedServiceId = requestedServiceId
        when (requestedServiceId) {
            getSongInformationRequestId -> { client?.serviceCall(lastRequestedServiceId, parameters, fragmentTrigger!!) }
            getAlbumInformationRequestId -> { client?.serviceCall(lastRequestedServiceId, parameters, fragmentTrigger!!) }
        }
        parameters.clear()
    }

    fun connectionError(inputMessage: String?, isRequestedCancellation: Boolean){
        val message = if(inputMessage.isNullOrBlank()) { "Sin conexión o problemas de conexión con el servido" } else inputMessage
        fragmentTrigger?.let {
            LoadingDialogManager.loader?.let {
                if (LoadingDialogManager.loader!!.isShowing){ LoadingDialogManager.loader!!.hide() }
            }
        }
    }

    private fun manageErrorOrException(responseCode: Int, response: String, message: String){
        LoadingDialogManager.loader?.let {
            if (LoadingDialogManager.loader!!.isShowing){ LoadingDialogManager.loader!!.hide() }
        }
        fragmentTrigger!!.presenter!!.showMessage(message)
    }

    fun universalCallBack(result: RestClient.CustomResult<String, String, Int, Int>): RestClient.CustomResult<String, String, Int, Int> {
        val successResponse = if(result.successResponse.contains("\n")) result.successResponse.replace("\n", "") else result.successResponse
        val errorResponse = result.errorResponse
        val requestResponseCode =  result.requestResponseCode
        val requestId = result.requestId

        when(requestResponseCode){
            200 -> {
                if (successResponse.isEmpty()){
                    manageErrorOrException(requestResponseCode, successResponse, "Respuesta vacía")
                }else{
                    when (requestId) {
                        RestClientConsumer.getSongInformationRequestId -> {
                            RestClientConsumer.lastRequestedServiceId = getSongInformationRequestId
                            fragmentTrigger?.let {
                                (fragmentTrigger as SongSearchFragment).fragmentPresenter!!.serviceCallback(JsonManager.getSearchResults(successResponse))
                            }
                        }
                        RestClientConsumer.getAlbumInformationRequestId -> {
                            RestClientConsumer.lastRequestedServiceId = getAlbumInformationRequestId
                            fragmentTrigger?.let {
                                (fragmentTrigger as AlbumInfoFragment).fragmentPresenter!!.serviceCallback(JsonManager.getAlbumInfoResponse(successResponse))
                            }
                        }
                        else -> {}
                    }
                }
            }
            500 -> { manageErrorOrException(requestResponseCode, successResponse, "Término no encontrado") }
            else -> { manageErrorOrException(requestResponseCode, errorResponse, "Error") }
        }
        return result
    }
}
package test.cl.itunessearcher.rest.jsonFormatter

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import test.cl.itunessearcher.models.Album
import test.cl.itunessearcher.models.Song

object JsonManager {

    private fun getSongInfo(data: JSONObject): Song?{
        if (data.has("kind") && data.getString("kind") == "song"){
            val currentElementInfo = HashMap<String, String>()
            currentElementInfo["name"] = if (data.has("trackName")) data.getString("trackName") else ""
            currentElementInfo["artist"] = if (data.has("artistName")) data.getString("artistName") else ""
            currentElementInfo["albumName"] = if (data.has("collectionName")) data.getString("collectionName") else ""
            currentElementInfo["albumId"] = if (data.has("collectionId")) data.getLong("collectionId").toString() else "0"
            return Song(currentElementInfo)
        }
        return null
    }

    fun getSearchResults(response: String): ArrayList<Any> {
        val results = ArrayList<Any> ()
        try {
            val rootJSONObject = JSONObject(response)
            if (rootJSONObject.has("results") && rootJSONObject["results"] is JSONArray && (rootJSONObject["results"] as JSONArray).length()>0){
                val resultsJSONArray = rootJSONObject.getJSONArray("results")
                for (index in 0..(resultsJSONArray.length() - 1)) {
                    val currentElement = resultsJSONArray.getJSONObject(index)
                    val song = getSongInfo(currentElement)
                    song?.let { results.add(it) }
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        return results
    }

    fun getAlbumInfoResponse(response: String): ArrayList<Any> {
        val results = ArrayList<Any> ()
        try {
            val rootJSONObject = JSONObject(response)
            if (rootJSONObject.has("results") && rootJSONObject["results"] is JSONArray && (rootJSONObject["results"] as JSONArray).length()>0){
                val resultsJSONArray = rootJSONObject.getJSONArray("results")
                val songs = ArrayList<Song> ()
                val album = Album()
                for (index in 0..(resultsJSONArray.length() - 1)) {
                    val currentElement = resultsJSONArray.getJSONObject(index)
                    if( (currentElement.has("wrapperType") && currentElement.getString("wrapperType") == "collection")){
                        val currentElementInfo = HashMap<String, String>()
                        currentElementInfo["name"] = if (currentElement.has("collectionName")) currentElement.getString("collectionName") else ""
                        currentElementInfo["artist"] = if (currentElement.has("artistName")) currentElement.getString("artistName") else ""
                        currentElementInfo["albumImageUrl"] = if (currentElement.has("artworkUrl100")) currentElement.getString("artworkUrl100") else ""
                        album.setAlbumInfo(currentElementInfo)
                    }else{
                        val song = getSongInfo(currentElement)
                        song?.let {
                            it.previewUrl = if(currentElement.has("previewUrl")) currentElement.getString("previewUrl") else ""
                            songs.add(it)
                        }
                    }
                }
                album.songsList = songs
                results.add(album)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        return results
    }



}
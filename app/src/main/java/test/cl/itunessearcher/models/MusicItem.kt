package test.cl.itunessearcher.models

abstract class MusicItem {

    var name : String = ""
    var artist : String = ""

    fun setBasicInfo(data: HashMap<String, String>){
        name = data["name"] as String
        artist = data["artist"] as String
    }

}
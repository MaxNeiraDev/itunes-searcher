package test.cl.itunessearcher.models

class Song(data: HashMap<String, String>): MusicItem() {

    var albumName : String = ""
    var previewUrl: String = ""
    var albumId: String = ""

    init {
        super.setBasicInfo(data)
        albumName = data["albumName"] as String
        albumId = data["albumId"] as String
    }

}
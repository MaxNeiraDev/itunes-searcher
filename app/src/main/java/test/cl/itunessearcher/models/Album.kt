package test.cl.itunessearcher.models

class Album(): MusicItem() {

    var albumImageUrl : String = ""
    var songsList: ArrayList<Song> ?= null

    fun setAlbumInfo(data: HashMap<String, String>){
        super.setBasicInfo(data)
        albumImageUrl = data["albumImageUrl"] as String
    }

}
package test.cl.itunessearcher.adapters

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import test.cl.itunessearcher.R
import test.cl.itunessearcher.models.Song
import java.util.ArrayList

class AlbumSongsListAdapter (var dataList: ArrayList<Song>,
                             private var listener: OnItemClickListener) : RecyclerView.Adapter<AlbumSongsListAdapter.CustomViewHolder>() {

    interface OnItemClickListener { fun onItemClick(item: Song, position: Int, view: View, holder: AlbumSongsListAdapter.CustomViewHolder) }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_album_song, parent, false)
        return CustomViewHolder(view!!)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, @SuppressLint("RecyclerView") position: Int) {
        holder.songName?.text = "Nombre: ${dataList[position].name}"
        holder.songNumber?.text = "${position+1}.-"
        holder.bind(dataList[position], listener, holder.itemView!!, holder)
    }

    override fun getItemCount(): Int { return dataList.size }

    open class CustomViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var songName: TextView ?= null
        var songNumber: TextView ?= null

        init {
            itemView.findViewById<View>(R.id.songName)?.let { songName = it as TextView }
            itemView.findViewById<View>(R.id.songNumber)?.let { songNumber = it as TextView }
        }

        fun bind(item: Song, listener: OnItemClickListener, view: View, holder: AlbumSongsListAdapter.CustomViewHolder) {
            itemView.setOnClickListener { listener.onItemClick(item, adapterPosition, view, holder) }
        }
    }

}

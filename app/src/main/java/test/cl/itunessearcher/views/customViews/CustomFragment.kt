package test.cl.itunessearcher.views.customViews

import android.app.Fragment
import android.content.Context
import android.os.Bundle
import android.support.annotation.NonNull
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.jetbrains.anko.longToast

abstract class CustomFragment : Fragment() {

    private var context: Context? = null
    private var view: View? = null
    private var layout: Int = 0
    var presenter: CustomFragmentPresenter? = null

    @NonNull
    protected abstract fun createPresenter(context: Context): CustomFragmentPresenter

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        context = container!!.context
        presenter = createPresenter(context!!)
        view = inflater?.inflate(layout, container, false)
        return view
    }

    fun errorCallback(message:String){
        longToast(message)
    }

    fun setLayout(id: Int) { layout = id }

    override fun getContext(): Context? { return context }

    override fun getView(): View? { return view }

}

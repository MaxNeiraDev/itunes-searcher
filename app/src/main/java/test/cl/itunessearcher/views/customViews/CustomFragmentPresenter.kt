package test.cl.itunessearcher.views.customViews

import android.app.Activity
import android.content.Context
import android.widget.Toast

abstract class CustomFragmentPresenter {

    var context: Context? = null

    abstract fun serviceCallback(data: ArrayList<Any>)

    abstract fun serviceCallback(data: HashMap<String, Any>)

    abstract fun serviceErrorCallback(code: String, message: String)

    fun showMessage(message: String) {
        if (context is Activity && !(context as Activity).isDestroyed && !(context as Activity).isFinishing) {
            Toast.makeText(context, message, Toast.LENGTH_LONG).show()
        }
    }

}
package test.cl.itunessearcher.views.fragments.songPreview.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import test.cl.itunessearcher.R
import test.cl.itunessearcher.views.customViews.CustomFragment
import test.cl.itunessearcher.views.customViews.CustomFragmentPresenter
import test.cl.itunessearcher.views.fragments.songPreview.presenter.SongPreviewFragmentPresenter

class SongPreviewFragment: CustomFragment() {

    var fragmentPresenter: SongPreviewFragmentPresenter? = null
    var previewUrl: String = ""

    override fun createPresenter(context: Context): CustomFragmentPresenter { return SongPreviewFragmentPresenter(this) }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setLayout(R.layout.fragment_song_preview)
        super.onCreateView(inflater, container, savedInstanceState)
        fragmentPresenter = presenter as SongPreviewFragmentPresenter

        fragmentPresenter!!.getSongViewWeb(previewUrl)

        return view
    }

}
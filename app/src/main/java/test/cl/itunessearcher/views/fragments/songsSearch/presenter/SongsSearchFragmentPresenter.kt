package test.cl.itunessearcher.views.fragments.songsSearch.presenter

import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.fragment_songs_search.*
import test.cl.itunessearcher.adapters.SongsListAdapter
import test.cl.itunessearcher.managers.FragmentsLoader
import test.cl.itunessearcher.managers.LoadingDialogManager
import test.cl.itunessearcher.managers.db.DatabaseManager
import test.cl.itunessearcher.models.Song
import test.cl.itunessearcher.rest.restConsumer.RestClientConsumer
import test.cl.itunessearcher.views.customViews.CustomFragmentPresenter
import test.cl.itunessearcher.views.fragments.albumInfo.view.AlbumInfoFragment
import test.cl.itunessearcher.views.fragments.songsSearch.view.SongSearchFragment

class SongsSearchFragmentPresenter (fragment: SongSearchFragment) : CustomFragmentPresenter() {

    private var view: SongSearchFragment? = null
    var songs = ArrayList<Song>()
    var offset = 0
    var lastSearch: String = ""
    var fullLoaded = false

    init {
        this.view = fragment
        super.context = view!!.context
    }

    override fun serviceCallback(data: ArrayList<Any>) {
        if (data.isNotEmpty()){
            when(RestClientConsumer.lastRequestedServiceId){
                RestClientConsumer.getSongInformationRequestId -> { manageSearchResult(data) }
            }
        }else{
            LoadingDialogManager.loader?.dismiss()
            if (offset>0 && songs.size>0)
                fullLoaded = true
            super.showMessage("No hay resultados")
        }
    }

    override fun serviceCallback(data: HashMap<String, Any>) {}

    override fun serviceErrorCallback(code: String, message: String) {}

    private fun manageSearchResult(data: ArrayList<Any>){
       data.forEach { songs.add(it as Song) }
       if(view!!.searchResults.adapter == null)
           setupList(view!!.searchResults, songs)
       else
           updateList()
       if (data.size % 20 != 0) fullLoaded = true
        view!!.searchListTitle.visibility = View.VISIBLE
       view!!.searchListTitle.text = "Resultados de la búsqueda"

        if(offset == 0){
            /**
             * Proceso incompleto. En este trozo de código se debe guardar en la bd local la
             * información de la búsqueda y los resultados de la misma, según sea el caso
             * correspondiente.
            * */
            /*var savedSongId: Long = -1
            var savedSearchId: Long = -1
            if(!DatabaseManager.searchExist(lastSearch)) {
                savedSearchId = DatabaseManager.saveSearch(lastSearch)
            }
            songs.map { savedSongId = DatabaseManager.saveSong(it) }
            if (savedSearchId != (-1).toLong() && savedSongId != (-1).toLong()) {
                DatabaseManager.saveSongSearchRelation(savedSearchId.toString(), savedSongId.toString())
            }*/
        }

       offset = songs.size
       LoadingDialogManager.loader!!.hide()
    }

    fun getSongInfo(search: String){
        lastSearch = search
        LoadingDialogManager.setupLoader(context!!)
        LoadingDialogManager.loader!!.setMessage("Buscando información...")
        LoadingDialogManager.loader!!.show()
        RestClientConsumer.getSongInformation(view!!, search, offset.toString())
    }

    fun setupList(list: RecyclerView, data: ArrayList<Song>){
        val recyclerListAdapter = SongsListAdapter(data, view!!)
        list.adapter = recyclerListAdapter
        (view!!.searchResults!!.adapter as SongsListAdapter).notifyDataSetChanged()
        view!!.searchResults.visibility = View.VISIBLE
    }

    fun showAlbumView(item: Song){
        val albumFragment = AlbumInfoFragment()
        albumFragment.albumId = item.albumId
        FragmentsLoader.loadFragment(view!!, albumFragment)
    }

    fun updateList(){
        (view!!.searchResults!!.adapter as SongsListAdapter).notifyDataSetChanged()
        view!!.searchResults!!.invalidate()
        view!!.searchResults!!.refreshDrawableState()
        val pos = if (offset != 0) offset+1 else offset
        view!!.searchResults!!.isNestedScrollingEnabled = true
        view!!.searchResults!!.layoutManager.scrollToPosition(pos)
    }

    fun resetList(){
        songs.clear()
        var offset = 0
        lastSearch = ""
        var fullLoaded = false
        (view!!.searchResults!!.adapter as SongsListAdapter).dataList.clear()
        (view!!.searchResults!!.adapter as SongsListAdapter).notifyDataSetChanged()
        view!!.searchResults!!.invalidate()
        view!!.searchResults!!.refreshDrawableState()
    }

}
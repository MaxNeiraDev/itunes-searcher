package test.cl.itunessearcher.views.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import test.cl.itunessearcher.R
import test.cl.itunessearcher.managers.FragmentsLoader
import test.cl.itunessearcher.managers.db.DatabaseManager
import test.cl.itunessearcher.views.fragments.songsSearch.view.SongSearchFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        FragmentsLoader.loadFragment(this, SongSearchFragment())
        DatabaseManager.setup(this.baseContext)
    }

}

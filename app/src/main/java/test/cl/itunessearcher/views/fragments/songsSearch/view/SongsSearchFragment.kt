package test.cl.itunessearcher.views.fragments.songsSearch.view

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.SearchView
import kotlinx.android.synthetic.main.fragment_songs_search.*
import test.cl.itunessearcher.R
import test.cl.itunessearcher.adapters.SongsListAdapter
import test.cl.itunessearcher.models.Song
import test.cl.itunessearcher.views.customViews.CustomFragment
import test.cl.itunessearcher.views.customViews.CustomFragmentPresenter
import test.cl.itunessearcher.views.fragments.songsSearch.presenter.SongsSearchFragmentPresenter

class SongSearchFragment: CustomFragment(), View.OnClickListener, SongsListAdapter.OnItemClickListener, SearchView.OnQueryTextListener {

    var fragmentPresenter: SongsSearchFragmentPresenter? = null
    private var listCustomScrollListener: SongSearchFragment.ListCustomScrollListener?= null

    override fun createPresenter(context: Context): CustomFragmentPresenter { return SongsSearchFragmentPresenter(this) }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setLayout(R.layout.fragment_songs_search)
        super.onCreateView(inflater, container, savedInstanceState)

        searchResults.setHasFixedSize(true)
        searchResults.setOnClickListener(this)
        searchResults.layoutManager = LinearLayoutManager(context)

        if(fragmentPresenter == null)
            fragmentPresenter = presenter as SongsSearchFragmentPresenter
        else
            fragmentPresenter!!.setupList(searchResults, fragmentPresenter!!.songs)

        listCustomScrollListener = SongSearchFragment.ListCustomScrollListener()
        listCustomScrollListener!!.view = this
        searchResults!!.addOnScrollListener(listCustomScrollListener)

        searchView.setOnQueryTextListener(this)
        setupSearchViewCloseButton()

        return view
    }

    override fun onClick(p0: View?) {}

    override fun onQueryTextSubmit(inputString: String?): Boolean {
        searchView.clearFocus()
        inputString?.let {
            val search = it.replace(" ", " + ")
            fragmentPresenter!!.getSongInfo(search)
            return true
        }
        return false
    }

    override fun onQueryTextChange(p0: String?): Boolean { return false }

    override fun onItemClick(item: Song, position: Int, view: View, holder: SongsListAdapter.CustomViewHolder) {
        fragmentPresenter!!.showAlbumView(item)
    }

    private fun setupSearchViewCloseButton(){
        val searchCloseButtonId = searchView.context.resources.getIdentifier("android:id/search_close_btn", null, null)
        val searchCloseButton = this.searchView.findViewById<ImageView>(searchCloseButtonId)
        searchCloseButton.setOnClickListener {
            fragmentPresenter!!.resetList()
            searchView.setQuery("", false)
        }
    }

    class ListCustomScrollListener : RecyclerView.OnScrollListener() {

        var view: SongSearchFragment?= null

        override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {
            var message = ""
            when (newState) {
                RecyclerView.SCROLL_STATE_IDLE -> message = "The RecyclerView is not scrolling"
                RecyclerView.SCROLL_STATE_DRAGGING -> message = "Scrolling now"
                RecyclerView.SCROLL_STATE_SETTLING -> message = "Scroll Settling"
            }
            if (!recyclerView!!.canScrollVertically(1)){
                if(view!!.fragmentPresenter!!.songs.size % 20 != 0 || !view!!.fragmentPresenter!!.fullLoaded){
                    view!!.fragmentPresenter!!.getSongInfo(view!!.fragmentPresenter!!.lastSearch)
                }else{
                    view!!.fragmentPresenter!!.showMessage("Se han cargado todos los elementos")
                }
            }
        }

        override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {}
    }

}
package test.cl.itunessearcher.views.fragments.songPreview.presenter

import android.webkit.WebView
import android.webkit.WebViewClient
import kotlinx.android.synthetic.main.fragment_song_preview.*
import test.cl.itunessearcher.managers.LoadingDialogManager
import test.cl.itunessearcher.views.customViews.CustomFragmentPresenter
import test.cl.itunessearcher.views.fragments.songPreview.view.SongPreviewFragment

class SongPreviewFragmentPresenter(fragment: SongPreviewFragment): CustomFragmentPresenter() {

    private var view: SongPreviewFragment? = null

    init {
        this.view = fragment
        super.context = view!!.context
    }

    override fun serviceCallback(data: ArrayList<Any>) {}

    override fun serviceCallback(data: HashMap<String, Any>) {}

    override fun serviceErrorCallback(code: String, message: String) {}

    fun getSongViewWeb(url: String){
        LoadingDialogManager.setupLoader(context!!)
        LoadingDialogManager.loader!!.setMessage("Cargando...")
        LoadingDialogManager.loader!!.show()
        view!!.webView.loadUrl(url)
        view!!.webView.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) { LoadingDialogManager.loader!!.dismiss() }
        }
    }

}
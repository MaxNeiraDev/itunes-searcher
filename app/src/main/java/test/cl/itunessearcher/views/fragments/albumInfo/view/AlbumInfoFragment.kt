package test.cl.itunessearcher.views.fragments.albumInfo.view

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_album_info.*
import test.cl.itunessearcher.R
import test.cl.itunessearcher.adapters.AlbumSongsListAdapter
import test.cl.itunessearcher.models.Song
import test.cl.itunessearcher.views.customViews.CustomFragment
import test.cl.itunessearcher.views.customViews.CustomFragmentPresenter
import test.cl.itunessearcher.views.fragments.albumInfo.presenter.AlbumInfoFragmentPresenter

class AlbumInfoFragment: CustomFragment(), View.OnClickListener, AlbumSongsListAdapter.OnItemClickListener {

    var fragmentPresenter: AlbumInfoFragmentPresenter? = null
    var albumId: String = ""

    override fun createPresenter(context: Context): CustomFragmentPresenter { return AlbumInfoFragmentPresenter(this) }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setLayout(R.layout.fragment_album_info)
        super.onCreateView(inflater, container, savedInstanceState)

        if (fragmentPresenter == null)
            fragmentPresenter = presenter as AlbumInfoFragmentPresenter

        if(fragmentPresenter!!.album == null)
            fragmentPresenter!!.getAlbumInfo(albumId)
        else{
            fragmentPresenter!!.loadAlbumImage()
            fragmentPresenter!!.setAlBumInfo()
            fragmentPresenter!!.setupList(songsList, fragmentPresenter!!.album!!.songsList!!)
        }

        songsList.setHasFixedSize(true)
        songsList.setOnClickListener(this)
        songsList.layoutManager = LinearLayoutManager(context)

        return view
    }

    override fun onClick(p0: View?) {
    }

    override fun onItemClick(item: Song, position: Int, view: View, holder: AlbumSongsListAdapter.CustomViewHolder) {
        fragmentPresenter!!.showSongPreviewView(item = item)
    }




}
package test.cl.itunessearcher.views.fragments.albumInfo.presenter

import android.graphics.Bitmap
import android.support.v7.widget.RecyclerView
import android.view.View
import com.budiyev.android.imageloader.ImageLoader
import kotlinx.android.synthetic.main.fragment_album_info.*
import test.cl.itunessearcher.adapters.AlbumSongsListAdapter
import test.cl.itunessearcher.managers.FragmentsLoader
import test.cl.itunessearcher.managers.LoadingDialogManager
import test.cl.itunessearcher.models.Album
import test.cl.itunessearcher.models.Song
import test.cl.itunessearcher.rest.restConsumer.RestClientConsumer
import test.cl.itunessearcher.views.customViews.CustomFragmentPresenter
import test.cl.itunessearcher.views.fragments.albumInfo.view.AlbumInfoFragment
import test.cl.itunessearcher.views.fragments.songPreview.view.SongPreviewFragment

class AlbumInfoFragmentPresenter(fragment: AlbumInfoFragment) : CustomFragmentPresenter() {

    private var view: AlbumInfoFragment? = null
    private var songs = ArrayList<Song>()
    var album: Album ?= null
    var albumImage: Bitmap?= null

    init {
        this.view = fragment
        super.context = view!!.context
    }

    override fun serviceCallback(data: ArrayList<Any>) {
        if(data.isNotEmpty()){
            album = data[0] as Album
            album!!.songsList?.map { songs.add(it) }
            album?.let {
                loadAlbumImage()
                setAlBumInfo()
                it.songsList?.let { setupList(view!!.songsList, it) }
            }
        }else{
            super.showMessage("Error en la carga de la información del álbum")
        }
        LoadingDialogManager.loader?.dismiss()
    }

    override fun serviceCallback(data: HashMap<String, Any>) {}

    override fun serviceErrorCallback(code: String, message: String) {}

    fun getAlbumInfo(id: String){
        LoadingDialogManager.setupLoader(context!!)
        LoadingDialogManager.loader!!.setMessage("Cargando información\n del álbum...")
        LoadingDialogManager.loader!!.show()
        RestClientConsumer.getAlbumInformation(view!!, id)
    }

    fun loadAlbumImage(){
        view!!.imageDownloadIndicator!!.visibility = View.VISIBLE
        if (albumImage == null){
            ImageLoader.with(context!!).from(album!!.albumImageUrl).onLoaded {
                view!!.activity.runOnUiThread {
                    view!!.albumImage!!.visibility = View.VISIBLE
                    view!!.imageDownloadIndicator!!.visibility = View.GONE
                    albumImage = it
                }
            }.onError {
                view!!.activity!!.runOnUiThread {
                    view!!.imageDownloadIndicator!!.visibility = View.GONE
                    view!!.albumImage!!.visibility = View.GONE
                }
            }.load(view!!.albumImage)
        }else{
            view!!.albumImage!!.setImageBitmap(albumImage)
            view!!.albumImage!!.visibility = View.VISIBLE
        }

    }

    fun setAlBumInfo(){
        view!!.albumName.text = album!!.name
        view!!.albumArtist.text = album!!.artist
    }

    fun setupList(list: RecyclerView, data: ArrayList<Song>){
        val recyclerListAdapter = AlbumSongsListAdapter(data, view!!)
        list.adapter = recyclerListAdapter
    }

    fun showSongPreviewView(item: Song){
        val songPreviewFragment = SongPreviewFragment()
        songPreviewFragment.previewUrl = item.previewUrl
        FragmentsLoader.loadFragment(view!!, songPreviewFragment)
    }

}
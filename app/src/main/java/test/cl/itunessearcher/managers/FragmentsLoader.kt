package test.cl.itunessearcher.managers

import android.app.Activity
import android.app.Fragment
import android.app.FragmentManager
import test.cl.itunessearcher.R

object FragmentsLoader {

    fun loadFragment(currentFragment: Fragment, newFragment: Fragment){
        loadView(currentFragment.activity.fragmentManager, newFragment)
    }

    fun loadFragment(currentActivity: Activity, newFragment: Fragment){
        loadView(currentActivity.fragmentManager, newFragment)
    }

    private fun loadView(fragmentManager: FragmentManager, newFragment: Fragment){
        val transaction = fragmentManager.beginTransaction()
        transaction?.replace(R.id.fragmentContainer, newFragment)?.addToBackStack(null)?.commit()
    }

}
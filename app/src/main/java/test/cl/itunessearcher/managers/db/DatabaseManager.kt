package test.cl.itunessearcher.managers.db

import android.annotation.SuppressLint
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.content.ContentValues
import test.cl.itunessearcher.models.Song

@SuppressLint("StaticFieldLeak")
object DatabaseManager {

    private var context: Context? = null
    private var database: SQLiteDatabase? = null
    private var databaseLoader: DatabaseLoader? = null

    fun setup(context: Context){
        DatabaseManager.context = context
        databaseLoader = DatabaseLoader(context)
        database = databaseLoader!!.openDatabase()
    }

    fun saveSong(song: Song): Long{
        val songValues = ContentValues()
        songValues.put("name", song.name)
        songValues.put("artist", song.artist)
        songValues.put("album_name", song.albumName)
        songValues.put("album_id", song.albumId)
        return database!!.insert("songs", null, songValues)
    }

    fun saveSearch(search: String): Long{
        val searchValues = ContentValues()
        searchValues.put("search", search)
        return database!!.insert("searches", null, searchValues)
    }

    fun saveSongSearchRelation(searchId: String, songId: String): Long{
        val idsValues = ContentValues()
        idsValues.put("song_id", songId)
        idsValues.put("search_id", searchId)
        return database!!.insert("results", null, idsValues)
    }

    fun getResults(searchId: String, songId: String):ArrayList<Song>{
        val parameters = arrayOf<String>(searchId, songId)
        val songs = ArrayList<Song>()
        val resultsWhereCursor = database!!.rawQuery("SELECT * FROM results WHERE song_id = ? AND search_id = ?",  parameters, null)
        if(resultsWhereCursor.count >0) {
            resultsWhereCursor.moveToFirst()
            val resultId= resultsWhereCursor.getString(resultsWhereCursor.getColumnIndex("song_id"))
            val params = arrayOf<String>(resultId)
            val songsWhereCursor = database!!.rawQuery("SELECT * FROM songs WHERE song_id = ?",  params, null)
            if(songsWhereCursor.count >0) {
                songsWhereCursor.moveToFirst()
                while (!songsWhereCursor.isAfterLast) {
                    val songInfo = HashMap<String, String>()
                    songInfo["name"] = songsWhereCursor.getString(songsWhereCursor.getColumnIndex("name"))
                    songInfo["artist"] = songsWhereCursor.getString(songsWhereCursor.getColumnIndex("artist"))
                    songInfo["albumName"] = songsWhereCursor.getString(songsWhereCursor.getColumnIndex("album_name"))
                    songInfo["albumId"] = songsWhereCursor.getString(songsWhereCursor.getColumnIndex("album_id"))
                    val song = Song(songInfo)
                    songs.add(song)
                    songsWhereCursor.moveToNext()
                }
                songsWhereCursor.close()
            }
        }
        resultsWhereCursor.close()
        return songs
    }

    fun songExist(inputId: String):Boolean{
        val songsWhereCursor = database!!.rawQuery("SELECT * FROM songs WHERE id = ?",  arrayOf<String>(inputId), null)
        val result = songsWhereCursor.count >0
        songsWhereCursor.close()
        return result
    }

    fun searchExist(inputId: String):Boolean{
        val songsWhereCursor = database!!.rawQuery("SELECT * FROM searches WHERE id = ?",  arrayOf(inputId), null)
        val result = songsWhereCursor.count >0
        songsWhereCursor.close()
        return result
    }

    fun clearAllInfo(){
        database!!.delete("results", "1", null)
        database!!.delete("songs", "1", null)
        database!!.delete("searches", "1", null)
    }

}
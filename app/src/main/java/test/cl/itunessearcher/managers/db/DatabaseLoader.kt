package test.cl.itunessearcher.managers.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class DatabaseLoader(var context: Context) {

    private val databaseName = "searches_local_cache.db"

    fun openDatabase(): SQLiteDatabase {
        val dbFile = context.getDatabasePath(databaseName)
        if (!dbFile.exists()) {
            try {
                val checkDB = context.openOrCreateDatabase(databaseName, Context.MODE_PRIVATE, null)
                checkDB?.close()
                copyDatabase(dbFile)
            } catch (e: IOException) {
                throw RuntimeException("Error creating source database", e)
            }
        }
        return SQLiteDatabase.openDatabase(dbFile.path, null, SQLiteDatabase.OPEN_READWRITE)
    }

    @Throws(IOException::class)
    private fun copyDatabase(dbFile: File) {
        val inputStream = context.assets.open(databaseName)
        val os = FileOutputStream(dbFile)
        val buffer = ByteArray(1024)
        while (inputStream.read(buffer) > 0) { os.write(buffer) }
        os.flush()
        os.close()
        inputStream.close()
    }

}
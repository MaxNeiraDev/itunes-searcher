package test.cl.itunessearcher.managers

import android.app.AlertDialog
import android.content.Context
import dmax.dialog.SpotsDialog

object LoadingDialogManager {

    var loader: AlertDialog? = null

    fun setupLoader(context: Context){
        loader = SpotsDialog.Builder().setContext(context).build()
        loader?.setCancelable(false)
    }

}